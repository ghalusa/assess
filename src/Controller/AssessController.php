<?php

namespace Drupal\assess\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Component\Utility\UrlHelper;

/**
 * Returns responses for Assessment routes.
 */
class AssessController extends ControllerBase {

  /**
   * Constructs an AssessController object.
   *
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The Account interface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(AccountInterface $current_user, MessengerInterface $messenger) {
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
    $this->utilities = \Drupal::service('assess.utilities');
    $this->results = \Drupal::service('assess.results');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('messenger')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

  /**
   * Dispatch Request
   *
   * @return array
   *   A render array.
   */
  public function dispatchRequest() {

    // // Test the linkify method in the Utilities class.
    // $string = 'This is a link to a website: https://www.example.com/.';
    // $linkified = $this->utilities->linkify($string);
    // echo $linkified;
    // die();

    $data = $settings = [];
    $message = $error = '';
    $response = new Response();
    $request = \Drupal::request();
    // e.g. foo=123&bar=true
    $post = $request->getContent();
    // Full path: /foo/bar
    $path_info = $request->getpathInfo();
    $arg = explode('/', $path_info);
    unset($arg[0]);
    $args = array_values($arg);
    // Query string: bar=yes&foo=no
    $query_string = $request->getQueryString();
    // URL parameter array: ["foo" => "no", "bar" => "yes"]
    $parameter_bag = $request->query;
    $url_parameters = $parameter_bag->all();

    // dump($path_info);
    // dd($args[1]);
    // dump($query_string);
    // dd($url_parameters);
    // dd($post);

    switch ($args[1]) {
      case 'results':
        $data = $this->results->assess_results_page( (int)$args[2] );
        return $data;
        break;
      case 'snapshot':

        dd('snapshot');

        break;
      case 'full':

        dd('full');

        break;
      case 'indy':

        dd('indy');

        break;
      case 'assess-ajax':

        dd('assess-ajax');

        break;
      case 'assess-leader':

        dd('assess-leader');

        break;
      case 'update-completed':

        dd('update-completed');

        break;
      case 'update-leaders':

        dd('update-leaders');

        break;
      case 'unconsensus':

        dd('unconsensus');

        break;
      default:
        // [GET] Test
        $data['content'] = [
          '#type' => 'item',
          '#markup' => $this->t('The AssessController works!'),
        ];
        return $data;
        break;

      return $data;
    }


    // // [GET] Get IIIF manifest.
    // if (in_array('manifest', $args) && !empty($args[1])) {
    //   // Prepare the manifest.
    //   $data = $this->record->prepare_iiif_manifest($args[1]);
    //   $headers = [
    //     'Access-Control-Allow-Headers' => 'Origin, X-Requested-With, Content-Type, Accept',
    //     'Access-Control-Allow-Methods' => 'GET',
    //     'Access-Control-Allow-Origin' => '*'
    //   ];
    //   // Prevent the manifests from being cached.
    //   \Drupal::service('page_cache_kill_switch')->trigger();
    //   // Return the IIIF manifest.
    //   return new JsonResponse($data, 200, $headers);
    // }

    // // [POST] Reproduction/Viewing Requests.
    // if (in_array('request', $args)) {
    //   $data = $this->reproductionViewingRequest->request_page_callback($args, $request->request->all());
    //   // If rendering in a modal, return as JSON.
    //   if ($request->request->get('modal') === 'true') {
    //     return new JsonResponse($data);
    //   } else {
    //     return $data;
    //   }
    // }

    // // [GET] Retrieve METS JSON for the Wellcome Viewer.
    // if (in_array('get_mets', $args)) {
    //   $data = $this->eadProcessor->get_mets($url_parameters);
    //   return new JsonResponse($data);
    // }

    // // [GET] Get name <div>.
    // if (in_array('get_name_div', $args)) {
    //   $data = $this->names->get_name_div($url_parameters);
    //   // Prevent the manifests from being cached.
    //   \Drupal::service('page_cache_kill_switch')->trigger();
    //   return new Response($data);
    // }

    // // [POST] Retrieve zipped files of collection images.
    // if (in_array('zip_files', $args)) {
    //   $data = $this->helper_methods->create_zip_of_images($request->request->get('files'));
    //   return new JsonResponse($data);
    // }

    // // [GET] Remove Rendered EAD HTML.
    // if (in_array('remove_ead_html', $args)) {
    //   $data = $this->helper_methods->_edan_extended_remove_rendered_ead_html();
    //   return new JsonResponse($data);
    // }

    // // [GET] Remove Rendered EAD HTML.
    // if (in_array('download_pdf_transcript', $args)) {
    //   $data = $this->helper_methods->_edan_extended_download_pdf_transcript();
    //   $headers = [
    //     'Content-type' => 'application/pdf',
    //     'Content-disposition' => 'inline',
    //   ];
    //   // Prevent from being cached.
    //   \Drupal::service('page_cache_kill_switch')->trigger();
    //   return new JsonResponse($data, 200, $headers);
    // }

    // dd($data);

    return $data;
  }

}
