<?php

namespace Drupal\assess\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an Assessment block.
 *
 * @Block(
 *   id = "assess",
 *   admin_label = @Translation("Assess"),
 *   category = @Translation("Assessment Tools")
 * )
 */
class AssessBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#markup' => $this->t('It works!'),
    ];
    return $build;
  }

}
