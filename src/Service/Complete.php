<?php

/**
 * @file
 * Contains \Drupal\assess\Service\Complete
 * This class handles the archiving and "snapshots" for the Assessments.
 */

namespace Drupal\assess\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Messenger\MessengerInterface;
use \DateTime;

class Complete {

  /**
   * Configurations for the assess module.
   */
  private $config;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * Constructor
   */
  public function __construct(RequestStack $request_stack, RendererInterface $renderer, MessengerInterface $messenger)
  {
    $this->config = \Drupal::config('assess.settings');
    $this->messenger = $messenger;
    $this->request = $request_stack->getCurrentRequest();
    $this->renderer = $renderer;
    $this->utilities = \Drupal::service('assess.utilities');
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('messenger'),
    );
  }

  public function assess_assessment_page() {
    
    $team = assess_get_assessment_nodes( 'team' );
    $role = assess_get_assessment_nodes( 'role' );
    $content = '';
    
    $block = module_invoke( 'block', 'block_view', 11 );
    $key = module_invoke( 'block', 'block_view', 14 );
    $share = module_invoke( 'assess_share', 'block_view', 'assessment_sharing' );
    $file = module_invoke( 'assess_share', 'block_view', 'assessment_doc' );
    
    $content .= '<div class="container ass-intro">' . 
            render( $block[ 'content' ] ) . render( $share[ 'content' ] ) . 
            render( $file[ 'content' ] ) . render( $key[ 'content' ] ) . '</div>'; 
    
    global $user;
    $group = NULL;
    $lid = NULL;
    if ( $user->uid > 0 ) {
      $group = assess_check_group( $user );
      $lid = assess_get_leader_id( $user->uid );
    }
    
    if ( $group ) {
      $content .= '<div id="consensus-wrapper" class="container">';
      $content .= '<div id="consensus">';
      $content .= 'Do the answers submitted for this assessment represent a consensus for the ' . $group[ 'title' ] . ' team participating in the LEAP learning collaborative?';
      $content .= '<br /><div class="check-group"><input type="checkbox" id="groupid" name="group" data-uid="' . $user->uid . '" data-lid="' . ( ( $lid ) ? $lid->lid : NULL ) . '" data-group="' . $group[ 'title' ] . '" ' . ( ( $lid ) ? 'checked' : NULL ) . ' /> <label for="groupid">YES, this is a consensus response.</label></div>';
      $content .= '</div>';
      $content .= '<div id="confirmation" class="container" style="display: none; text-align:center;">Please confirm that this assessment reflects consensus answers for ' . $group[ 'title' ] . '<br />';
      $content .= '<a href="#" id="leader-confirm" data-gid="' . $group[ 'gid' ] . '" data-uid="' . $user->uid . '">Yes</a>';
      $content .= '<a href="#" id="leader-cancel">No</a></div>';
      $content .= '<div id="unconfirmation" class="container" style="display: none; text-align:center;">Please confirm that this assessment does <strong>NOT</strong> reflect consensus for  ' . $group[ 'title' ] . ' team<br />';
      $content .= '<a href="#" id="leader-unconfirm" data-lid="' . ( ( $lid ) ? $lid->lid : NULL ) . '" data-uid="' . $user->uid . '">Yes</a>';
      $content .= '<a href="#" id="leader-uncancel">No</a></div>';
      $content .= '<div id="unconfirmation-message" class="container" style="display: none;"></div>';
      $show = ( $lid ) ? NULL : ' style="display: none;"';
      $link = l( 'You can access your completed assessments on your dashboard', 'dashboard' );
      $content .= '<span class="btn" id="completed" ' . $show . '>Mark assessment as completed</span>';
      $content .= '<br><br><div id="complete-intro"' . $show . '>Checking the "Mark Assessment as Complete" button will archive your assessment answers and notify the LEAP team that this assessment is complete. ' . $link . '</div>';
      $content .= '</div>';
    }
    
    if ( $team ) {
      $content .= '<header class="assessment-type"><h2 class="container">Build the Team</h2></header>';
      $content .= assess_create_full_assessment( $team ); 
    }
    if ( $role ) {
      $content .= '<header class="assessment-type"><h2 class="container">Do the Work</h2></header>';
      $content .= assess_create_full_assessment( $role ); 
    }
   
    return $content;
    
  } // public function assess_assessment_page()

  public function assess_indy_page() {
    
    $indy = assess_get_assessment_nodes( 'independent' );
    //print_r($indy);
    //$role = assess_get_assessment_nodes( 'role' );
    $content = '';
    
    $block = module_invoke( 'block', 'block_view', 11 );
    $key = module_invoke( 'block', 'block_view', 14 );
    $share = module_invoke( 'assess_share', 'block_view', 'assessment_sharing' );
    $file = module_invoke( 'assess_share', 'block_view', 'assessment_doc' );

  /**
    $content .= '<div class="container ass-intro">' . 
            render( $block[ 'content' ] ) . render( $share[ 'content' ] ) . 
            render( $file[ 'content' ] ) . render( $key[ 'content' ] ) . '</div>'; 
   **/         
    $content .= '<div class="container ass-intro">' . '</div>'; 
    
    global $user;
    $group = NULL;
    $lid = NULL;
    if ( $user->uid > 0 ) {
      $group = assess_check_group( $user );
      $lid = assess_get_leader_id( $user->uid );
    }
    
    
    if ( $indy ) {
      //$content .= '<header class="assessment-type"><h2 class="container">Assessment</h2></header>';
      $content .= assess_create_full_indy_assessment( $indy ); 
    }
    
    return $content;
    
  } // public function assess_indy_page()

  /**
   * 
   */
  public function assess_create_full_assessment( $nids, $uid = NULL ) {
    
    $content = '';
    $forms = array();
    foreach ( $nids as $value ) {
      $forms[ $value->nid ] = assess_assessment( $value->nid, $uid );
    }
    
    if ( !empty( $forms ) ) {
      $content .= assess_full_sort_forms( $forms );
    }
    
    return $content;
    
  } // public function assess_create_full_assessment()

  public function assess_create_full_indy_assessment( $nids, $uid = NULL ) {
    
    $content = '';
    $forms = array();
    foreach ( $nids as $value ) {
      $forms[ $value->nid ] = assess_assessment( $value->nid, $uid );
    }
    
    if ( !empty( $forms ) ) {
      $content .= assess_full_sort_indy_forms( $forms );
    }
    
    return $content;
    
  } // public function assess_create_full_indy_assessment()

  /**
   * 
   */
  public function assess_full_sort_forms( $forms ) {
    //print_r($forms);
    $topics = '';
    foreach ( $forms as $key => $value ) {
      $topic = assess_get_node_title( $key );
      $count = assess_get_assessment_count( $key );
      $title = format_plural( $count, 'Assessment', 'Assessments' ) . ' for ' . $topic;
      $link = l( 'View topic', 'node/' . $key );
      
      $content = $value;
      
      $topics .= theme( 'assess-full', array( 'forms' => $content, 'title' => $title, 'link' => $link, 'nid' => $key ) );
    }
    
    return $topics;
    
  } // public function assess_full_sort_forms()

  public function assess_full_sort_indy_forms( $forms ) {
    //print_r($forms);
    $topics = '';
    foreach ( $forms as $key => $value ) {
      $topic = assess_get_node_title( $key );
      $count = assess_get_assessment_count( $key );
      //$title = format_plural( $count, 'Assessment', 'Assessments' ) . ' for ' . $topic;
      //$link = l( 'View topic', 'node/' . $key );
      
      $content = $value;
      
      $topics .= theme( 'assess-full', array( 'forms' => $content, 'title' => $title, 'link' => $link, 'nid' => $key ) );
    }
    
    return $topics;
    
  } // public function assess_full_sort_forms()

  /**
   * 
   */
  public function assess_load_ask() {
    
    drupal_add_js( drupal_get_path( 'module', 'assess' ) . '/js/ask.js', array( 'every_page' => FALSE ) );
    
    $block = module_invoke( 'webform', 'block_view', 'client-block-111' );
    $form = render( $block[ 'content' ] );
    return theme( 'assess-ask', array( 'form' => $form ) );
    
  } // public function assess_load_ask()

  /**
   * Set a cookie after they get the lightbox
   */
  public function assess_full_cookie() {
    
    setcookie( 'pct_full', TRUE, time() + 31536000, '/' );
    
  } // public function assess_full_cookie()

  /**
   * Returns the number of Assessment questions associated with a node
   * 
   * @param int $nid
   *   The node ID for the question
   */
  public function assess_get_assessment_count( $nid ) {
    
    $node = node_load( $nid );
    $assess = field_get_items( 'node', $node, 'field_assessment' );
    
    return count( $assess );
    
  } // public function assess_get_assessment_count()

  /**
   * Check the user to see if they are a member of a group
   * 
   * @param int $user
   *   The User object
   * 
   * @return
   *   Returns the first user group
   */
  public function assess_check_group( $user ) {
    
    $groups = og_get_groups_by_user( $user );
    
    if ( array_key_exists( 'node', $groups ) ) {
      reset( $groups[ 'node' ] );
      $group = node_load( key( $groups[ 'node' ] ) );
      
      return array( 'gid' => $group->nid, 'title' => $group->title );
    }
    
  } // public function assess_check_group()

}
