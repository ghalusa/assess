<?php

/**
 * @file
 * Contains \Drupal\assess\Service\Consensus
 * Description: None
 */

namespace Drupal\assess\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Messenger\MessengerInterface;
use \DateTime;

class Consensus {

  /**
   * Configurations for the assess module.
   */
  private $config;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * Constructor
   */
  public function __construct(RequestStack $request_stack, RendererInterface $renderer, MessengerInterface $messenger)
  {
    $this->config = \Drupal::config('assess.settings');
    $this->messenger = $messenger;
    $this->request = $request_stack->getCurrentRequest();
    $this->renderer = $renderer;
    $this->utilities = \Drupal::service('assess.utilities');
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('messenger'),
    );
  }

  public function assess_unconsensus( $lid, $uid ) {
    
    $check = assess_unconsensus_get_consensus_uid( $lid );
    
    if ( $check->uid == $uid ) {
      assess_unconsensus_unset_consensus( $lid, $uid );
      assess_unconsensus_unset_leader( $lid );
      assess_unconsensus_unset_leader( $lid );
      drupal_json_output( array( 'status' => 'success', 'msg' => t( 'Unset consensus status' ) ) );
    }
    else {
      drupal_json_output( array( 'status' => 'success', 'msg' => t( 'There was a problem.' ) ) );
    }
    
  } // public function assess_unconsensus()


  /**
   * Return the user ID for a given leader ID
   * 
   * @param int $lid
   *   The leader ID
   * 
   * @return int
   *   Returns the User ID
   */
  public function assess_unconsensus_get_consensus_uid( $lid ) {
    
    $result = db_query( 'SELECT uid FROM {assess_leader} WHERE lid = :lid', array( ':lid' => $lid ) )->fetch();
    
    return $result;
    
  } // public function assess_unconsensus_get_consensus_uid()


  /**
   * Unset any consensus answers that are not part of a completed assessment
   * 
   * @param int $lid
   *   The leader ID
   */
  public function assess_unconsensus_unset_consensus( $lid, $uid ) {
    
    $aids = db_select( 'assess_data', 'a' )
            ->fields( 'a', array( 'aid' ) )
            ->condition( 'uid', $uid )
            ->condition( 'completed', 1, '<' )->execute()->fetchCol();
    if ( $aids ) {
      db_delete( 'assess_group_answers' )
        ->condition( 'aid', array_values( $aids ), 'IN')
        ->condition( 'lid', $lid )->execute();
    }
    
  } // public function assess_unconsensus_unset_consensus()


  /**
   * Set the leader status as inactive
   * 
   * @param int $lid
   *   The leader ID
   */
  public function assess_unconsensus_unset_leader( $lid ) {
    
    $rows = db_update( 'assess_leader' )
            ->fields( array(
              'active' => 0
            ))
            ->condition( 'lid', $lid )
            ->execute();
    
    return $rows;
    
  } // public function assess_unconsensus_unset_leader()


  /**
   * Update the users previous answers when a user becomes a consensus leader
   */
  public function assess_consensus_update( $lid ) {
    
    $ids = json_decode( filter_input( INPUT_POST, 'ids' ) );
    
    $query = db_insert( 'assess_group_answers' )->fields( array( 'lid', 'aid' ) );
    
    foreach ( $ids as $aid ) {
      $query->values( array(
        'lid' => $lid,
        'aid' => $aid,
      ) );
    }
    
    $query->execute();
    
    drupal_json_output( 'Answers updated' );
    
  } // public function assess_consensus_update()

}
