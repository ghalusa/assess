<?php

/**
 * @file
 * Contains \Drupal\assess\Service\Utilities
 * Various utility methods for the Assess custom module.
 */

namespace Drupal\assess\Service;

use Symfony\Component\HttpFoundation\RequestStack;

class Utilities {

  /**
   * Configurations for the assess module.
   */
  private $config;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Constructor
   */
  public function __construct(RequestStack $request_stack)
  {
    $this->config = \Drupal::config('assess.settings');
    $this->request = $request_stack->getCurrentRequest();
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
    );
  }

  /**
   * Foo Processor
   * Foo bar processor.
   */
  public function foo_processor($array = []) {

    $data = [];

    if(!empty($data)) {
      // Do stuff
    }

    return $data;
  }

  /**
   * Automatically links URLs found in a string.
   * Source: https://github.com/jmrware/LinkifyURL
   *
   * @param text 
   *   The string to search for URLs.
   */
  public function linkify( $text ) {
      $url_pattern = '/(\()((?:ht|f)tps?:\/\/[a-z0-9\-._~!$&\'()*+,;=:\/?#[\]@%]+)(\))
        |
        (\[)
        ((?:ht|f)tps?:\/\/[a-z0-9\-._~!$&\'()*+,;=:\/?#[\]@%]+)
        (\])
        |
        (\{)
        ((?:ht|f)tps?:\/\/[a-z0-9\-._~!$&\'()*+,;=:\/?#[\]@%]+)
        (\})
        |
        (<|&(?:lt|\#60|\#x3c);)
        ((?:ht|f)tps?:\/\/[a-z0-9\-._~!$&\'()*+,;=:\/?#[\]@%]+)
        (>|&(?:gt|\#62|\#x3e);)
        |
        (
        (?: ^
        | [^=\s\'"\]]
        ) \s*[\'"]?
        | [^=\s]\s+
        )
        ( \b
        (?:ht|f)tps?:\/\/
        [a-z0-9\-._~!$\'()*+,;=:\/?#[\]@%]+
        (?:
        (?!
        &(?:gt|\#0*62|\#x0*3e);
        | &(?:amp|apos|quot|\#0*3[49]|\#x0*2[27]);
        [.!&\',:?;]?
        (?:[^a-z0-9\-._~!$&\'()*+,;=:\/?#[\]@%]|$)
        ) &
        [a-z0-9\-._~!$\'()*+,;=:\/?#[\]@%]*
        )*
        [a-z0-9\-_~$()*+=\/#[\]@%]
        )
        /imx';

      $url_replace = '$1$4$7$10$13<a href="$2$5$8$11$14" title="Opens in a new tab or window" target="_blank">$2$5$8$11$14</a>$3$6$9$12';

      return preg_replace($url_pattern, $url_replace, $text);
  }

  /**
   * Format bytes to human readable format
   * EX: 1048576 => 1MB
   *
   * @param a_bytes 
   *   Byte number
   */
  public function format_bytes($a_bytes) {
    if ($a_bytes < 1024) {
      return $a_bytes .' B';
    } elseif ($a_bytes < 1048576) {
      return round($a_bytes / 1024, 2) .' KB';
    } elseif ($a_bytes < 1073741824) {
      return round($a_bytes / 1048576, 2) . ' MB';
    } elseif ($a_bytes < 1099511627776) {
      return round($a_bytes / 1073741824, 2) . ' GB';
    } elseif ($a_bytes < 1125899906842624) {
      return round($a_bytes / 1099511627776, 2) .' TB';
    } elseif ($a_bytes < 1152921504606846976) {
      return round($a_bytes / 1125899906842624, 2) .' PB';
    } elseif ($a_bytes < 1180591620717411303424) {
      return round($a_bytes / 1152921504606846976, 2) .' EB';
    } elseif ($a_bytes < 1208925819614629174706176) {
      return round($a_bytes / 1180591620717411303424, 2) .' ZB';
    } else {
      return round($a_bytes / 1208925819614629174706176, 2) .' YB';
    }
  }

}
