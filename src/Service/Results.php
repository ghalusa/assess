<?php

/**
 * @file
 * Contains \Drupal\assess\Service\Results
 * Helper functions for the All Assessments page.
 */

namespace Drupal\assess\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityInterface;
use \DateTime;

class Results {

  /**
   * Configurations for the assess module.
   */
  private $config;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * Constructor
   */
  public function __construct(RequestStack $request_stack, RendererInterface $renderer, MessengerInterface $messenger)
  {
    $this->config = \Drupal::config('assess.settings');
    $this->messenger = $messenger;
    $this->request = $request_stack->getCurrentRequest();
    $this->renderer = $renderer;
    $this->utilities = \Drupal::service('assess.utilities');
    $this->all = \Drupal::service('assess.all');
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('messenger'),
    );
  }

  public function assess_results_page($id = 0) {
    
    if (!$id) {
      $redirect = new RedirectResponse('/assessment/full');
      $redirect->send();
    }
    
    $content = '';

    $date = $this->assess_get_date( $id );
    
    if ( $date ) {
      $date_formatted = \Drupal::service('date.formatter')->format($date->date, 'short');
      $content .= '<div id="full-date"><div class="container">Updated on: <span>' . $date_formatted . '</span></div></div>';
    }
    
    // $intro = module_invoke( 'block', 'block_view', 13 );

    $block = \Drupal\block_content\Entity\BlockContent::load(1);

    if(isset($block) && !empty($block)) {
      $block_rendered = \Drupal::entityTypeManager()->getViewBuilder('block_content')->view($block);
    }


    dd($block_rendered);
    
    $content .= '<div class="container ass-intro">' . render( $intro[ 'content' ] ) . '</div>';
    
    $team = assess_get_assessment_nodes( 'team' );
    $role = assess_get_assessment_nodes( 'role' );
    
    if ( $team ) {
      $teams = assess_check_answers( $team, $id );
      $content .= '<header class="assessment-type"><h2 class="container">Build the Team</h2></header>';
      $content .= assess_create_full_assessment( $teams, $id ); 
    }
    if ( $role ) {
      $roles = assess_check_answers( $role, $id );
      $content .= '<header class="assessment-type"><h2 class="container">Do the Work</h2></header>';
      $content .= assess_create_full_assessment( $roles, $id ); 
    }

    drupal_add_js( drupal_get_path( 'module', 'assess' ) . '/js/results.js', array( 'every_page' => FALSE) );
    
    if ( !$content ) {
      drupal_goto( 'assessment/full' );
    }
   
    return $content;
    
  } // public function assess_assessment_page()


  /**
   * Get the date of the latest question
   * 
   * @param string $id
   *   The sid to query for
   */
  public function assess_get_date( $id ) {
    
    $db = \Drupal\Core\Database\Database::getConnection();

    if ( is_numeric( $id ) ) {
      $id_field = 'uid';
    }
    else {
      $id_field = 'sid';
    }

    $result = $db->select('assess_data','a')
      ->fields('a', ['date'])
      ->condition( $id_field, $id, '=' )
      ->range( 0, 1 )
      ->orderBy( 'date', 'DESC' )
      ->execute()
      ->fetch();
    
    return $result;
   
  } // public function assess_get_date()

  /**
   * Check to see if there is an answer for the given node by the given user
   * 
   * @param array $nodes
   *   An array of Assessment node IDs
   * @param string $id
   *   The user ID or assessment cookie
   * 
   * @return
   *   Returns an array of node IDs
   */
  public function assess_check_answers( $nodes, $id ) {
    
    $results = array();
    
    foreach ( $nodes as $key => $value ) {
      $check = assess_get_previous( $value->nid, $value->node_field_data_field_assessment_nid, $id );
      if ( $check ) {
        $results[] = $value;
      }
    }
    
    return $results;
    
  } // public function assess_check_answers()


  /**
   * Check that there is an answer for the given node and id
   * 
   * @param int $nid
   *   Node ID of the Assessment
   * @param string $id
   *   Either the user ID or Assessment cookie ID
   * 
   * @return
   *   Returns an Answer ID
   */
  public function assess_get_previous( $nid, $qid, $id ) {
    
    $query = db_select( 'assess_data', 'a' );
    $query->addField( 'a', 'aid' );
    
    if ( is_numeric( $id ) ) {
      $query->condition( 'uid', $id );
    }
    else {
      $query->condition( 'sid', $id );
    }
    
    $query->condition( 'nid', $nid );
    $query->condition( 'qid', $qid );
    
    $result = $query->execute()->fetch();
    
    return $result;
    
  } // public function assess_get_previous()

}
