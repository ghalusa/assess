<?php

/**
 * @file
 * Contains \Drupal\assess\Service\Main
 * Description: None
 */

namespace Drupal\assess\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Messenger\MessengerInterface;
use \DateTime;

class Main {

  /**
   * Configurations for the assess module.
   */
  private $config;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * Constructor
   */
  public function __construct(RequestStack $request_stack, RendererInterface $renderer, MessengerInterface $messenger)
  {
    $this->config = \Drupal::config('assess.settings');
    $this->messenger = $messenger;
    $this->request = $request_stack->getCurrentRequest();
    $this->renderer = $renderer;
    $this->utilities = \Drupal::service('assess.utilities');
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('messenger'),
    );
  }

  /**
   * Creates the Assessment tab or block
   */
  public function assess_assessment( $nid, $uid = NULL ) {
    
    $node = node_load( $nid );
   
    $content = assess_create_assesssment( $node, $uid );

    return $content;
    
  } // public function assess_assessment()


  /**
   * Creates the Assessment form or returns the users previous answer
   * 
   * @param object $node
   *   The node object of the Module
   * 
   * @return 
   *   Returns the themed Assessment form or the theme answer
   */
  public function assess_create_assesssment( $node, $uid = NULL ) {
    
    $sid = filter_input( INPUT_COOKIE, 'assessment' );
    if ( ! $sid ) {
      assess_set_cookie();
    }
    
    $content = '';
    $assessment = field_get_items( 'node', $node, 'field_assessment' );
    if ( ! empty( $assessment ) ) {
      foreach ( $assessment as $assess ) {
        $content .= assess_get_forms( $assess[ 'nid' ], $node->nid, $uid );
      }
    }
    
    return $content;
    
  } // public function assess_create_assessment()


  /**
   * 
   */
  public function assess_get_forms( $nid, $node_id, $user_id = NULL ) {
    
    if ( $user_id && ! is_numeric( $user_id ) ) {
      $sid = $user_id;
      $uid = NULL;
    }
    else {
      global $user;
      $uid = ( $user_id ) ? $user_id : $user->uid;
      $sid = ( $user->uid > 0 ) ? NULL : filter_input( INPUT_COOKIE, 'assessment' );
    }
    
    $lid = ( $uid ) ? assess_get_leader_id( $uid ) : NULL;
      
    // Get the questions to be displayed
    $assess_node = node_load( $nid );
    $question_array = field_get_items( 'node', $assess_node, 'field_questions' );
    $questions = assess_get_questions( $question_array );
    
    $previous = assess_get_answer( $questions[ 0 ][ 'qid' ], $node_id, $uid, $sid );
    
    return theme( 'assessment', array( 
        'nid' => $node_id, 
        'question' => $questions[ 0 ][ 'question' ],
        'answers' => $questions[ 0 ][ 'answers' ],
        'qid' => $questions[ 0 ][ 'qid' ],
        'lid' => ( $lid ) ? $lid->lid : NULL,
        'aid' => ( is_object( $previous ) ) ? $previous->aid : NULL,
        'previous' => ( is_object( $previous ) ) ? $previous->answer : 0,
        )
      );
    
  } // public function assess_get_forms()


  /**
   * Returns the Assessment questions for the Module
   * 
   * @param object $question_array
   *   The field_assessement node reference object
   * 
   * @return
   *   Return an array of questions to be displayed
   */
  public function assess_get_questions( $question_array ) {
   
    $type = 'scale';
    
    foreach ( $question_array as $value ) {
      $question_node = field_collection_item_load( $value[ 'value' ] );
      $question = field_get_items( 'field_collection_item', $question_node, 'field_question' );
      $answer_array = field_get_items( 'field_collection_item', $question_node, 'field_answers' );
      $responses = field_get_items( 'field_collection_item', $question_node, 'field_response' );
      $response = ( empty( $responses ) ) ? NULL : $responses[ 0 ][ 'value' ];
      
      $answers = assess_parse_answers( $answer_array );
      
      $qid = assess_lookup_question_nid( $value[ 'value' ] );
      $questions[] = array(
        'question' => $question[ 0 ][ 'value' ],
        'qid' => $qid->entity_id,
        'answers' => $answers,
        'response' => $response,
        'type' => $type,
      );
    }
    
    return $questions;
    
  } // public function assess_get_questions()


  /**
   * Create answers for the different Question types
   * 
   * @param array $answer_array
   *   An array containing the answers
   * 
   * @param string $type
   *   The question type
   * 
   * @return 
   *   Returns an array of answers
   */
  public function assess_parse_answers( $answer_array, $type = 'scale' ) {
    
    switch ( $type ) {
      case 'scale':
        foreach( $answer_array as $value ) {
          $answers[] = $value[ 'value' ];
        }
        
        return $answers;
    }
    
  } // public function assess_parse_answers()


  /**
   * Inserts Assessment answers into the database
   * 
   * @param array $answers
   *   An array of answer vlaues
   */
  public function assess_insert_answers( $answers ) {
   
    $aid = db_insert( 'assess_data' )
            ->fields( $answers )
            ->execute();
    
    return $aid;
    
  } // public function assess_insert_answers()


  /**
   * Returns previous answers from the db
   * 
   * @param int $qid
   *   The question id
   * 
   * @param int $nid
   *   The Module/Topic node ID
   * 
   * @param int $uid
   *   The user ID
   * 
   * @param string $sid
   *   The cookied unique ID
   * 
   * @return
   *   Returns an answer object or NULL
   */
  public function assess_get_answer( $qid, $nid, $uid = NULL, $sid = NULL ) {

    $query = db_select( 'assess_data', 'a' );
    $query->fields( 'a' );
    $query->condition( 'a.qid', $qid );
    $query->condition( 'a.nid', $nid );
    
    if ( $uid ) {
      $query->condition( 'a.uid', $uid );
    }
    else {
      $query->condition( 'a.sid', $sid );    
    }
    
    $query->condition( 'a.completed', 'IS NULL' );
    
    $query->orderBy( 'a.date', 'DESC' );
    
    $results = $query->execute()->fetch();

    return $results;
    
  } // public function assess_get_answers()


  /**
   * Return a node title by node ID
   * 
   * @param int $nid
   *   The node ID
   * 
   * @return string
   *   The node title
   */
  public function assess_get_node_title( $nid ) {
    
    $query = db_select( 'node', 'n' );
    $query->addField( 'n', 'title' );
    $query->condition( 'nid', $nid  );
   
    $result = $query->execute()->fetchField();
    
    return $result;
    
  } // public function assess_get_node_title()


  /**
   * Sets a cookie with a unique ID
   */
  public function assess_set_cookie() {
    
    $val = uniqid( 'assess' );
    setcookie( 'assessment', $val, time() + 3600 * 24 * 30, '/' );
    
  } // public function assess_set_cookie()


  /**
   * Creates the assessment sharing block
   */
  public function assess_sharing() {
    
    global $user;
    
    if ( arg( 0 ) == 'assessment' && arg( 1 ) == 'results' && arg( 2 ) ) {
      $id = arg( 2 );
    }
    elseif ( $user->uid ) {
      $id = $user->uid;
    }
    elseif ( filter_input( INPUT_COOKIE, 'assessment' ) ) {
      $id = filter_input( INPUT_COOKIE, 'assessment' );
    }
    else {
      return '';
    }
    
    $url = 'assessment/results/' . $id;
    
    return $url;
    
  } // public function assess_sharing()


  /**
   * An ajax callback
   * Here we parse the form values
   * Return the thank you
   */
  function _assess_feedback_form_callback( $form, &$form_state )  {
    
    $flag = FALSE;
    if ( empty( $form_state[ 'values' ][ 'name' ] ) ) {
      $flag = TRUE;
    }
    elseif ( empty( $form_state[ 'values' ][ 'email' ] )  ) {
      $flag = TRUE;
    }
    elseif ( empty( $form_state[ 'values' ][ 'feedback' ] )  ) {
      $flag = TRUE;
    }
    
    if ( $flag ) {
      $commands = array();
      $commands[] = ajax_command_html( '#feedback-errors', theme_status_messages( array( 'error' ) ) );
      $commands[] = ajax_command_html( '#feedback-block', drupal_render( drupal_get_form( 'assess_feedback_form' ) ) );
      
      return array( '#type' => 'ajax', '#commands' => $commands );
    }
    
    $node = node_load( 88 );
    global $user;
    
    $title = assess_get_node_title( $form_state[ 'values' ][ 'module' ] );
    
    $data = array(
      1 => array( 'value' => array( $form_state[ 'values' ][ 'name' ] ) ),
      2 => array( 'value' => array( $form_state[ 'values' ][ 'email' ] ) ),
      3 => array( 'value' => array( $form_state[ 'values' ][ 'feedback' ] ) ),
      4 => array( 'value' => array( $title ) ),
    );

    $submission = (object) array(
      'nid' => 88,
      'uid' => $user->uid,
      'submitted' => REQUEST_TIME,
      'remote_addr' => ip_address(),
      'is_draft' => FALSE,
      'data' => $data,
    );

    module_load_include( 'inc', 'webform', 'includes/webform.submissions' );
    webform_submission_insert( $node, $submission );
    webform_submission_send_mail( $node, $submission );

    $confirmation = check_markup( $node->webform[ 'confirmation' ], $node->webform[ 'confirmation_format' ], '', TRUE );

    return array( '#markup' => $confirmation );
    
  } // public function assess_feedback_form_callback()


  /**
   * Field Collection
   * We need the question id
   * Get the Node ID
   * 
   * @param int $fcid
   *   The field collection ID
   * 
   * @return
   *   Returns the Node ID of the Field Collection Question
   */
  public function assess_lookup_question_nid( $fcid ) {
    
    $query = db_select( 'field_data_field_questions', 'q' );
    $query->addField( 'q', 'entity_id' );
    $query->condition( 'field_questions_value', $fcid );
    
    $result = $query->execute()->fetch();
    
    return $result;
    
  } // public function assess_lookup_question_nid()


  /**
   * 
   */
  public function assess_user_block() {
    
    global $user;
    
    if ( $user->uid > 0 ) {
      return assess_create_user_block( $user->uid  );
    }
    
    return FALSE;
    
  } // public function assess_user_block()


  /**
   * 
   */
  public function assess_create_user_block( $uid ) {
    
    $link = l( 'Go to Your Assessment Results', 'assessment/results/' . $uid );
    $total = assess_total_topics();
    
    $total_answers = assess_get_user_answer_count( $uid );
      
    return theme( 'assess-user-block', array( 'answers' => $total_answers, 'total' => $total, 'link' => $link ) );
    
  } // public function assess_create_user_block()


  /**
   * Gets the node ids from the View
   * 
   * @returns array
   *   Returns an array of Node IDs
   */
  public function assess_get_assessment_nodes( $display ) {
    $view = views_get_view( 'assessments' );
    
    if ( $view && $view->access( $display ) ) {
      $view->set_display( $display );
      $view->pre_execute();
      $view->execute();
      
      return $view->result;
    }
    
  } // public function assess_get_assessment_nodes()


  /**
   * Returns the number of Topics that the user has answered
   */
  public function assess_get_user_answer_count( $uid ) {
    
    $query = db_select( 'assess_data', 'ad' );
    $query->addField( 'ad', 'qid' );
    $query->condition( 'uid', $uid );
    $query->distinct();
    $result = $query->countQuery()->execute()->fetchField();
    
    return $result;
    
  } // public function assess_get_user_answer_count()


  /**
   * Returns the number of Topic nodes
   */
  public function assess_total_topics() {
    
    $query = db_select( 'node', 'n' );
    $query->addField( 'n', 'nid' );
    $query->join( 'node_revision', 'nr', 'nr.vid = n.vid' );
    $query->condition( 'n.type', 'module' );
    $query->condition( 'n.status', 1 );
    
    $result = $query->countQuery()->execute()->fetchField();
    
    return $result;
    
  } // public function assess_total_topics()


  /**
   * Ajax callback
   * 
   * @param int $nid
   *   The Assessment node ID
   * @param int $qid
   *   The Question ID
   * @param int $answer
   *   The answer
   * 
   * @return
   *   Returns the answer ID from the database
   */
  public function assess_assessment_ajax( $nid, $qid, $answer, $lid = NULL ) {
    
    global $user;
    
    $data = array();
    $data[ 'uid' ] = ( $user->uid > 0 ) ? $user->uid : NULL;
    $data[ 'sid' ] = filter_input( INPUT_COOKIE, 'assessment' );
    $data[ 'qid' ] = $qid;
    $data[ 'nid' ] = $nid;
    $data[ 'date' ] = time();
    $data[ 'answer' ] = $answer;
    
    $aid = assess_insert_answers( $data );
    
    if ( $lid ) {
      assess_update_leaderboard( $lid, $aid );
    }
    
    drupal_json_output( $aid );
    
  } // functioin assess_assessment_ajax()


  /**
   * Update the Leader table with the answer
   * 
   * @param int $lid
   *   The leader ID
   * 
   * @param int $aid
   *   The answer ID
   */
  public function assess_update_leaderboard( $lid, $aid ) {
    
    db_insert( 'assess_group_answers' )
      ->fields( array( 'lid' => $lid, 'aid' => $aid ) )
      ->execute();
    
  } // public function assess_update_leaderboard()


  /**
   * Return the leader ID for a given User
   * 
   * @param int $uid
   *   The user ID
   */
  public function assess_get_leader_id( $uid ) {
    
    $lid = db_query( 'SELECT lid FROM {assess_leader} WHERE uid = :uid AND active = 1 ORDER BY date DESC LIMIT 1', array( ':uid' => $uid ) )->fetch();
    
    return $lid;
    
  } // public function assess_get_leader_id()


  /**
   * Return the leader ID for a given group
   * 
   * @param int $gid
   *   The group ID
   */
  public function assess_get_leader_id_by_group( $gid ) {
    
    $lid = db_query( 'SELECT lid, uid FROM {assess_leader} WHERE gid = :gid AND active = 1 ORDER BY date DESC LIMIT 1', array( ':gid' => $gid ) )->fetch();
    
    return $lid;
    
  } // public function assess_get_leader_id()


  /**
   * 
   */
  public function assess_leader_ajax( $uid, $gid )  {
    
    $check = assess_get_leader_id( $uid );
    
    if ( $check ) {
      $lid = $check->lid;
    }
    else {
      $lid = assess_insert_leader( $uid, $gid );
    }
    
    drupal_json_output( $lid );
    
  } // assess_leader_ajax()


  /**
   * Insert the user into the leader table
   * 
   * @param int $uid
   *   The user ID
   * 
   * @param int $gid
   *   The group ID
   * 
   * @return
   *   Returns the leader ID
   */
  public function assess_insert_leader( $uid, $gid ) {
    
    $date = time();
    $lid = db_insert( 'assess_leader' )
            ->fields( array( 'uid' => $uid, 'gid' => $gid, 'date' => $date ) )
            ->execute();
    
    return $lid;
    
  } // function assess_inser_leader()

}
