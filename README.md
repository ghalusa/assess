# Assessment Module

## Contents of this File

* Introduction
* Installation
* Maintainers

### Introduction

The Assessment module for [The Primary Care Team Guide website](http://improvingprimarycare.org/), MacColl Center for Health Care Innovation.

### Installation

* Install as you would normally install a contributed Drupal module.
   See: [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

### Maintainers/Support

* [Strategic Content Services](https://strategiccontent.com/)
* Goran Halusa: [ghalusa@gmail.com](ghalusa@gmail.com)
